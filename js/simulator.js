var Simulator = {};

var quantum = 10;
var slowConstant = 1000;
var executationTime = 0;

var executing = -1;
var io = 'VAZIA';

var done = process;

var source   = $("#entry-template").html();
var template = Handlebars.compile(source);

Simulator.next = function() {
	executing += 1;

	if(executing == done.length) {
		return false;
		executing = 0;
	}
	
	if((!done[executing] || !done[executing].maxTimes)) {
		return false;
	}
	
	Simulator.execute(executing);
};

Simulator.execute = function(pid) {
	var time = quantum;

	if(process[pid].time < quantum) {
		time = process[pid].time;
		process[pid].time = 0;
	} else {
		process[pid].time -= quantum;
	}

	time *= slowConstant;

	setTimeout(function() {
		if(process[pid].timeIo) {
			io = process[pid].name;
			
			

			setTimeout(function() {
				io = 'VAZIA';

				executationTime += process[pid].timeIo * slowConstant;

				Simulator.log();
			}, process[pid].timeIo * slowConstant);
		}
		
		executationTime += time;
		Simulator.next();
	}, time);

	Simulator.log();
};

Simulator.getDone = function() {
	var text = [];

	for (var i = (executing - 1); i >= 0; i--) {
		if(process[i].name != io) {
			text.push(process[i].name);
		}
	};

	for (var i = (process.length - 1); i > executing; i--) {
		if(process[i].name != io) {
			text.push(process[i].name);
		}
	};

	return text.join(', ');
};

Simulator.log = function() {
	$('#console').append(template({
		executationTime: executationTime / slowConstant,
		doneList: Simulator.getDone(),
		executing: process[executing].name,
		suspended: io
	}));
};

Simulator.next();
