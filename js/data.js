var process = [
	{
		name: 'P1',
		time: 3,
		timeIo: 2,
		maxTimes: 3
	},
	{
		name: 'P2',
		time: 8,
		maxTimes: 2
	},
	{
		name: 'P3',
		time: 8,
		timeIo: 3,
		maxTimes: 10
	},
	{
		name: 'P4',
		time: 7,
		maxTimes: 1
	},
	{
		name: 'P5',
		time: 10,
		maxTimes: 4
	}
];
